Codely Plugin Documentação
==========================

O propósito deste repositório é responder todas as questões a:
1. Por que o [Codely](https://gitlab.com/jcfaracco/codely) precisa de plugins?
2. Como ele (o plugin) funciona?
3. Quais as habilidades de programação são necessárias?

Introdução
----------

Codely é um programa genérico para gerar código baseado em blocos lógicos. Ele é predominantemente construído em Python e Javascript, usando um framework do Google chamado [Blockly](https://developers.google.com/blockly/). A principal ideia do projeto é prover uma camada de abstração baseada em blocos visuais para que qualquer um possa utilizá-los para gerar códigos específicos.

Em outras palavras, o Codely se baseia na ideia de um gerador de código muito semelhante a um compilador. O usuário programa de forma visual e o projeto gera o código referente a lõgica. Porém, por questões técnicas óbvias, ele não é um compilador. Cada bloco tem sua própria estrutura que contém uma porção de código com uma função.

Essa porção é de responsabilidade de cada Plugin. Se um usuário quer gerar blocos correspondentes a sua linguagem, ele precisa desenvolver seu próprio Plugin que a represente.

Blockly tem algumas linguagens préconfiguradas, mas elas não estão disponíveis no Codely. Primordialmente, porque se o usuário quiser tentar, ele não precisará do projeto. Basta acessar um exemplo do Blockly chamado de ["Code Editor"](https://blockly-demo.appspot.com/static/demos/code/index.html).


Estrutura
---------

Os Plugins nada mais são que uma coleção de arquivos textos configurados em uma estrutura de diretórios padrão. Além disso, o Codely possui seu local padrão de procura por Plugins. São eles: `/etc/codely/plugins/` e `~/.codely/plugins/`.

Considerando um Plugin instalado/copiado dentro do diretório $HOME do usuário, a estrutura de um Plugin segue:

```
/home/user/.codely/plugins/MyPluginName/ .------> plugin.cfg
                                         |
                                         |------> setup/ .------> clean
                                         |               |
                                         |               |------> pre
                                         |               |
                                         |               |------> post
                                         |               |
                                         |               `------> run
                                         |
                                         |------> blocks/ .-----> blocks.json
                                         |                |
                                         |                `-----> lang.json
                                         |
                                         `------> code/ --------> [...]
```

* `plugin.cfg`: este arquivo contem as diretrizes e configurações básicas do plugin. Deve se iniciar sempre a seção de configuração com a entrada `[Plugin]`. Esta seção, possui os seguintes atributos:
    * `name` (obrigatório): refere-se ao nome do Plugin que será exibido na aba referente ao código do mesmo.
    * `language` (obrigatório): qual tipo de linguagem de programação predominantemente utilizada em cada bloco.
    * `main` (obrigatório): nome do programa principal gerado pelo projeto Codely no espaço temporário.
    * `app` (opcional): comando para algum serviço, aplicativo, daemon ou outros necessários para a execução do programa gerado.
* `setup/`: diretório com scripts utilizados para configuração da execução do programa gerado**.
    * `pre`: script com comandos utilizados *antes* da execução do código.
    * `run`: script com comandos para execução do código.
    * `post`: script com comandos utilizados *depois* da execução do código.
    * `clean`: script com comandos para limpeza da execução.
* `blocks/`: diretório com a descrição dos blocos gerados pela ferramenta [BlockFactory](https://blockly-demo.appspot.com/static/demos/blockfactory/index.html) do Google e suas respectivas traduções.
    * `blocks.json`: arquivo com todos os blocos novos utilizados pelo Plugin. Cada bloco é separado por virgula, dentro de uma estrutura de chaves "{..., ..., ...}". Assim, o formato e estrutura do arquivo se mantém como JSON.
    * `lang.json`: outro arquivo JSON com a mesma estrutura do arquivo anterior, mas cada seção especifica a linguagem e dentro dela a mensagem traduzida para cada bloco de acordo com seu respectivo ID.
* `code/`: diretório com a implementação de código para cada bloco: novos e já existentes no Codely.

A seção `code` contém os arquivos com a implementação de cada bloco. Para associar os arquivos de código com cada bloco, é necessário criar cada arquivo com o mesmo nome do identificador do bloco. O bloco referente a função *while* por exemplo. Seu bloco tem identificador **controls_whileUntil**, logo o nome do arquivo referente ao código do bloco *while* deve ter esse nome.

Algumas diretivas são essenciais para poder dar essas habilidades ao seu código. Nesse caso, somente 3 são necessárias: `BLOCK`, `FIELD` e `VALUE`. Ainda tomando o bloco *while* como exemplo, esse bloco aceita uma condição para permitir o laço e aninhamneto de blocos dentro da estrutura. Nesse caso requerem respectivamente um `VALUE` pro laço e um `BLOCK` pro aninhamento. Todas elas devem ser separadas por dupla chaves. Tomando a linguagem C como exemplo, uma implementação possível para esse bloco seria:

```
$ cat <<EOF > code/controls_whileUntil
while({{ VALUE }}) {
    {{ BLOCK }}
}
EOF
```

Por fim, somente o conhecimento referente a linguagem que será implementada pelos blocos existentes e novos é exigida. Na pior das hipóteses será necessário ter a noção de onde incluir as diretivas mencionadas acima para prover a funcionalidade aos blocos no processamento do código final.